#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
	mkfifo("/tmp/loggerfifo",0777);
	int fd=open("/tmp/loggerfifo", O_RDONLY);

	int salir=0;

	while(salir==0)
	{
		char buff[200];
		read(fd,buff,sizeof(buff));
		printf("%s\n", buff);
		
		//Salida del bucle infinito para cerrar el logger cuando se cierra el tenis.

		if(buff[0]=='-')
		{
			printf("----Tenis cerrado. Cerrando logger...--- \n");
			salir=1;
		}
	}

	close(fd);
	unlink("/tmp/loggerfifo");
	return 0;
}

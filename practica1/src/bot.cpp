
#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"

int main()
{
	//Creamos el fichero, el puntero a la memoria y el puntero para la proyeccion
	int file;
	DatosMemCompartida* pMemComp;
	char* proyeccion;
	//Abrimos el fichero
	file=open("/tmp/datosBot.txt",O_RDWR);
	//Proyectamos el fichero
	proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);
	//Cerramos el fichero
	close(file);
	//Apuntamos nuestro puntero de Datos a la proyeccion del fichero en memoria
	pMemComp=(DatosMemCompartida*)proyeccion;

	//Condicion de salida
	int salir=0;

	//Acciones de control de la raqueta
	while(salir==0)
	{
		if (pMemComp->accion==5)
		{
			salir=1;
		}
		float posRaqueta;
		posRaqueta=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		
		if(posRaqueta<pMemComp->esfera.centro.y)
			pMemComp->accion=1;

		if(posRaqueta>pMemComp->esfera.centro.y)
			pMemComp->accion=-1;
		else
			pMemComp->accion=0;
		
		//usleep(25000);  //¿Como implementar el usleep?¿Para que?
	}
	//Desmontamos la proyeccion de memoria
	munmap(proyeccion,sizeof(*(pMemComp)));

}
